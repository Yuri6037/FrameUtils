#ifndef CINTERFACE_COMMON_H
#define CINTERFACE_COMMON_H

#include <libavutil/rational.h>

typedef void * handle_t;

typedef struct rgb_s
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
} rgb_t;

typedef struct rgb_pos_s
{
    unsigned int stream_index;
    int x;
    int y;
} rgb_pos_t;

typedef struct stream_properties_s
{
    AVRational time_base;
    int64_t bit_rate;
    int bit_rate_tolerance;
    int ticks_per_frame;
    int delay;
    int width;
    int height;
    int coded_width;
    int coded_height;
    int gop_size;
    AVRational frame_rate;
    int max_b_frames;
    float b_quant_factor;
    float b_quant_offset;
    int has_b_frames;
    float i_quant_factor;
    float i_quant_offset;
} stream_properties_t;

#endif