#pragma once

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>

typedef struct stream_s
{
    AVCodecContext *dec;
    AVFrame *cur;
    uint8_t *rgb_buffer;
    int rgb_buffer_stride;
    struct SwsContext *sws_ctx;
} stream_t;

typedef struct decoder_s
{
    AVFormatContext *ctx;
    stream_t *streams;
    AVPacket *pck;
} decoder_t;

typedef struct encoder_s
{
    AVFormatContext *ctx;
    stream_t *streams;
    unsigned int stream_count;
    AVPacket *pck;
} encoder_t;

#define DECODER(handle) decoder_t *self = (decoder_t *)handle
#define ENCODER(handle) encoder_t *self = (encoder_t *)handle

#define MY_EOF -541478725
