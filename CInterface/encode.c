#include "structs.h"
#include "encode.h"

static encoder_t *alloc_encoder()
{
    encoder_t *res = av_malloc(sizeof(encoder_t));
    if (!res)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return NULL;
    }
    res->ctx = NULL;
    res->streams = NULL;
    res->stream_count = 0;
    return res;
}

handle_t ffmpeg_encode_new(const char *file, const char *format)
{
    encoder_t *enc = alloc_encoder();
    if (!enc)
        return NULL;
    if (avformat_alloc_output_context2(&enc->ctx, NULL, NULL, format) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Could not create output context\n");
        return NULL;
    }
    //Defect of libavformat you give it a filename but it's too stupid to open it; nice!
    if (avio_open(&enc->ctx->pb, file, AVIO_FLAG_WRITE) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to open file in write mode\n");
        return NULL;
    }
    enc->pck = av_packet_alloc();
    if (!enc->pck)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return NULL;
    }
    return enc;
}

static unsigned int alloc_new_stream(encoder_t *self)
{
    unsigned int idx = self->stream_count;
    self->stream_count += 1;
    if (self->streams == NULL)
        self->streams = av_calloc(self->stream_count, sizeof(stream_t));
    else
        self->streams = av_realloc(self->streams, self->stream_count * sizeof(stream_t));
    if (self->streams)
    {
        self->streams[idx].sws_ctx = NULL;
        self->streams[idx].dec = NULL;
        self->streams[idx].cur = NULL;
        self->streams[idx].rgb_buffer = NULL;
        self->streams[idx].rgb_buffer_stride = 0;
    }
    return idx;
}

static void assign_codec_props(AVCodecContext *ctx, const stream_properties_t *props)
{
    ctx->time_base = props->time_base;
    ctx->bit_rate = props->bit_rate;
    ctx->bit_rate_tolerance = props->bit_rate_tolerance;
    ctx->ticks_per_frame = props->ticks_per_frame;
    ctx->delay = props->delay;
    ctx->width = props->width;
    ctx->height = props->height;
    ctx->coded_width = props->coded_width;
    ctx->coded_height = props->coded_height;
    ctx->gop_size = props->gop_size;
    ctx->framerate = props->frame_rate;
    ctx->max_b_frames = props->max_b_frames;
    ctx->b_quant_factor = props->b_quant_factor;
    ctx->b_quant_offset = props->b_quant_offset;
    ctx->has_b_frames = props->has_b_frames;
    ctx->i_quant_factor = props->i_quant_factor;
    ctx->i_quant_offset = props->i_quant_offset;
}

static AVFrame *alloc_frame(const AVCodecContext *ctx)
{
    AVFrame *frame = av_frame_alloc();
    if (!frame)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to allocate frame\n");
        return NULL;
    }
    frame->width = ctx->width;
    frame->height = ctx->height;
    frame->format = ctx->pix_fmt;
    av_log(NULL, AV_LOG_INFO, "Allocating frame buffer of size %dx%d\n", frame->width, frame->height);
    int res;
    if ((res = av_frame_get_buffer(frame, 0)) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to allocate frame buffer %d\n", res);
        return NULL;
    }
    return frame;
}

static int align_16(int in)
{
    while (in % 32 != 0)
        in += 1;
    return in;
}

static struct SwsContext *get_sws_context(encoder_t *enc, unsigned int stream_index)
{
    if (!enc->streams[stream_index].sws_ctx)
    {
        AVCodecContext *ctx = enc->streams[stream_index].dec;
        int stride = align_16(ctx->width * 3);
        uint8_t *buffer = av_malloc(stride * ctx->height);
        if (!buffer)
            return NULL;
        enc->streams[stream_index].rgb_buffer = buffer;
        enc->streams[stream_index].rgb_buffer_stride = stride;
        struct SwsContext *sws = sws_getContext(ctx->width, ctx->height, AV_PIX_FMT_RGB24, ctx->width, ctx->height, ctx->pix_fmt, SWS_BICUBIC, NULL, NULL, NULL);
        enc->streams[stream_index].sws_ctx = sws;
        return sws;
    }
    return enc->streams[stream_index].sws_ctx;
}

int ffmpeg_encode_new_stream_from_existing(handle_t handle, handle_t decoder, unsigned int in_stream_index, unsigned int *out_stream_index)
{
    ENCODER(handle);
    decoder_t *dec = (decoder_t *)decoder;
    unsigned int idx = alloc_new_stream(self);
    if (self->streams == NULL)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return 0;
    }
    AVStream *out_stream = avformat_new_stream(self->ctx, NULL);
    if (!out_stream)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to create new stream\n");
        return 0;
    }
    AVStream *in_stream = dec->ctx->streams[in_stream_index];
    if (avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to copy stream parameters\n");
        return 0;
    }
    *out_stream_index = idx;
    return 1;
}

int ffmpeg_encode_new_stream(handle_t handle, const stream_properties_t *props, unsigned int *stream_index)
{
    ENCODER(handle);
    unsigned int idx = alloc_new_stream(self);
    if (self->streams == NULL)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return 0;
    }
    const AVCodec *codec = avcodec_find_encoder(AV_CODEC_ID_H264);
    if (!codec)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to find H264 encoder\n");
        return 0;
    }
    AVStream *stream = avformat_new_stream(self->ctx, NULL);
    if (!stream)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to create new stream\n");
        return 0;
    }
    AVCodecContext *ctx = avcodec_alloc_context3(codec);
    if (!ctx)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to create new codec context\n");
        return 0;
    }
    assign_codec_props(ctx, props);
    ctx->pix_fmt = codec->pix_fmts[0];
    if (self->ctx->oformat->flags & AVFMT_GLOBALHEADER)
        ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    if (avcodec_open2(ctx, codec, NULL) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to open codec\n");
        return 0;
    }
    if (avcodec_parameters_from_context(stream->codecpar, ctx) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to transfer codec parameters to stream\n");
        return 0;
    }
    stream->time_base = props->time_base;
    self->streams[idx].cur = alloc_frame(ctx);
    if (!self->streams[idx].cur)
        return 0;
    self->streams[idx].dec = ctx;
    *stream_index = idx;
    return 1;
}

int ffmpeg_encode_copy_streams(handle_t handle, handle_t decoder, unsigned int in_stream_index, unsigned int out_stream_index)
{
    ENCODER(handle);
    decoder_t *dec = (decoder_t *)decoder;
    while (1)
    {
        int res = av_read_frame(dec->ctx, dec->pck);
        if (res < 0 && res != MY_EOF)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to read packet\n");
            return 0;
        }
        if (res == MY_EOF)
            break;
        if (dec->pck->stream_index == in_stream_index)
        {
            dec->pck->stream_index = (int)out_stream_index;
            if (av_interleaved_write_frame(self->ctx, dec->pck) < 0)
            {
                av_log(NULL, AV_LOG_ERROR, "Failed to write packet\n");
                return 0;
            }
            av_packet_unref(dec->pck);
        }
    }
    return 1;
}

int ffmpeg_encode_new_frame(handle_t handle, unsigned int stream_index, int64_t timestamp)
{
    ENCODER(handle);
    if (av_frame_make_writable(self->streams[stream_index].cur) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Unable to make frame writable\n");
        return 0;
    }
    if (!get_sws_context(self, stream_index))
        return 0;
    memset(self->streams[stream_index].rgb_buffer, 0, self->streams[stream_index].rgb_buffer_stride * self->streams[stream_index].dec->height);
    self->streams[stream_index].cur->pts = timestamp;
    return 1;
}

void ffmpeg_encode_set_rgb(handle_t handle, const rgb_pos_t *pos, const rgb_t *rgb)
{
    ENCODER(handle);
    uint8_t *buffer = self->streams[pos->stream_index].rgb_buffer;
    int stride = self->streams[pos->stream_index].rgb_buffer_stride;
    buffer[(pos->y * stride) + (pos->x * 3)] = rgb->r;
    buffer[(pos->y * stride) + (pos->x * 3 + 1)] = rgb->g;
    buffer[(pos->y * stride) + (pos->x * 3 + 2)] = rgb->b;
}

static int write_frame(encoder_t *self, AVCodecContext *enc, AVFrame *frame)
{
    if (avcodec_send_frame(enc, frame) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to send frame to encoder\n");
        return 0;
    }
    int ret = 0;
    while (ret >= 0)
    {
        ret = avcodec_receive_packet(enc, self->pck);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return 1;
        else if (ret < 0)
        {
            av_log(NULL, AV_LOG_ERROR, "Error during encoding\n");
            return 0;
        }
        av_log(NULL, AV_LOG_ERROR, "Write packet %"PRId64" (size=%d)\n", self->pck->pts, self->pck->size);
        if (av_interleaved_write_frame(self->ctx, self->pck) < 0)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to write packet\n");
            return 0;
        }
        av_packet_unref(self->pck);
    }
    return 1;
}

int ffmpeg_encode_write_frame(handle_t handle, unsigned int stream_index)
{
    ENCODER(handle);
    AVCodecContext *enc = self->streams[stream_index].dec;
    uint8_t *buffer = self->streams[stream_index].rgb_buffer;
    AVFrame *frame = self->streams[stream_index].cur;
    struct SwsContext *sws_ctx = get_sws_context(self, stream_index);
    const uint8_t *src_buffers[4] = {
            buffer,
            buffer,
            buffer,
            buffer
    };
    const int src_strides[4] = {
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride
    };
    if (sws_scale(sws_ctx, src_buffers, src_strides, 0, enc->height, frame->data, frame->linesize) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to convert RGB to encoder frame\n");
        return 0;
    }
    return write_frame(self, enc, frame);
}

int ffmpeg_encode_flush(handle_t handle, unsigned int stream_index)
{
    ENCODER(handle);
    AVCodecContext *enc = self->streams[stream_index].dec;
    return write_frame(self, enc, NULL);
}

int ffmpeg_encode_write_trailer(handle_t handle)
{
    ENCODER(handle);
    if (av_write_trailer(self->ctx) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to write trailer\n");
        return 0;
    }
    return 1;
}

int ffmpeg_encode_write_header(handle_t handle)
{
    ENCODER(handle);
    if (avformat_write_header(self->ctx, NULL) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to write header\n");
        return 0;
    }
    return 1;
}

void ffmpeg_encode_delete(handle_t handle)
{
    ENCODER(handle);
    if (self->streams)
    {
        for (unsigned int i = 0; i < self->ctx->nb_streams; i++)
        {
            if (self->streams[i].sws_ctx)
            {
                sws_freeContext(self->streams[i].sws_ctx);
                av_free(self->streams[i].rgb_buffer);
            }
            av_frame_free(&self->streams[i].cur);
            avcodec_close(self->streams[i].dec);
        }
        av_free(self->streams);
    }
    av_packet_free(&self->pck);
    avformat_free_context(self->ctx);
    av_free(handle);
}
