#ifndef CINTERFACE_ENCODE_H
#define CINTERFACE_ENCODE_H

#include "common.h"

handle_t ffmpeg_encode_new(const char *file, const char *format);
int ffmpeg_encode_new_stream(handle_t handle, const stream_properties_t *props, unsigned int *stream_index);
int ffmpeg_encode_write_header(handle_t handle);
int ffmpeg_encode_write_trailer(handle_t handle);
int ffmpeg_encode_new_frame(handle_t handle, unsigned int stream_index, int64_t timestamp);
int ffmpeg_encode_write_frame(handle_t handle, unsigned int stream_index);
int ffmpeg_encode_flush(handle_t handle, unsigned int stream_index);
void ffmpeg_encode_set_rgb(handle_t handle, const rgb_pos_t *pos, const rgb_t *rgb);
void ffmpeg_encode_delete(handle_t handle);

int ffmpeg_encode_new_stream_from_existing(handle_t handle, handle_t decoder, unsigned int in_stream_index, unsigned int *out_stream_index);
int ffmpeg_encode_copy_streams(handle_t handle, handle_t decoder, unsigned int in_stream_index, unsigned int out_stream_index);

#endif