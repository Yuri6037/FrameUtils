#ifndef CINTERFACE_DECODE_H
#define CINTERFACE_DECODE_H

#include "common.h"

#include <bits/stdint-intn.h>
#include <bits/stdint-uintn.h>

handle_t ffmpeg_decode_new(const char *file);
int ffmpeg_decode_init_streams(handle_t handle);
unsigned int ffmpeg_decode_count_streams(handle_t handle);
int ffmpeg_decode_is_video_stream(handle_t handle, unsigned int index);
int ffmpeg_decode_frame(handle_t handle, unsigned int *stream_index);
int ffmpeg_decode_frame_to_rgb(handle_t handle, unsigned int stream_index);
void ffmpeg_decode_get_rgb(handle_t handle, const rgb_pos_t *pos, rgb_t *out);
void ffmpeg_decode_delete(handle_t handle);
void ffmpeg_decode_get_frame_size(handle_t handle, unsigned int stream_index, unsigned int *w, unsigned int *h);
int64_t ffmpeg_decode_get_frame_timestamp(handle_t handle, unsigned int stream_index);
void ffmpeg_decode_get_stream_props(handle_t handle, unsigned int stream_index, stream_properties_t *props);

#endif //CINTERFACE_DECODE_H
