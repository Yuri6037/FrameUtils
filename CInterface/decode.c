#include "decode.h"
#include "structs.h"

#include <libswscale/swscale.h>

#include <stdio.h>

static decoder_t *alloc_decoder()
{
    decoder_t *res = av_malloc(sizeof(decoder_t));
    if (!res) {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return NULL;
    }
    res->ctx = NULL;
    res->streams = NULL;
    return res;
}

//Returns 0 if failed, 1 if succeeded
int ffmpeg_decode_init_streams(handle_t handle)
{
    DECODER(handle);
    self->streams = av_calloc(self->ctx->nb_streams, sizeof(stream_t));
    if (!self->streams)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return 0;
    }
    for (unsigned int i = 0; i < self->ctx->nb_streams; i++)
    {
        AVStream *stream = self->ctx->streams[i];
        const AVCodec *codec = avcodec_find_decoder(stream->codecpar->codec_id);
        if (!codec)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to find decoder for stream #%u\n", i);
            return 0;
        }
        AVCodecContext *ctx = avcodec_alloc_context3(codec);
        if (!ctx)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to allocate the decoder context for stream #%u\n", i);
            return 0;
        }
        if (avcodec_parameters_to_context(ctx, stream->codecpar) < 0)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to copy decoder parameters to input decoder context for stream #%u\n",
                   i);
            return 0;
        }
        if (ctx->codec_type == AVMEDIA_TYPE_VIDEO || ctx->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            if (ctx->codec_type == AVMEDIA_TYPE_VIDEO)
                ctx->framerate = av_guess_frame_rate(self->ctx, stream, NULL);
            if (avcodec_open2(ctx, codec, NULL) < 0)
            {
                av_log(NULL, AV_LOG_ERROR, "Failed to open decoder for stream #%u\n", i);
                return 0;
            }
        }
        self->streams[i].dec = ctx;
        self->streams[i].cur = av_frame_alloc();
        self->streams[i].sws_ctx = NULL;
        if (!self->streams[i].cur)
        {
            av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
            return 0;
        }
    }
    return 1;
}

unsigned int ffmpeg_decode_count_streams(handle_t handle)
{
    DECODER(handle);
    return self->ctx->nb_streams;
}

int ffmpeg_decode_is_video_stream(handle_t handle, unsigned int index)
{
    DECODER(handle);
    return (self->streams[index].dec->codec_type == AVMEDIA_TYPE_VIDEO) ? 1 : 0;
}

int ffmpeg_decode_frame(handle_t handle, unsigned int *stream_index)
{
    DECODER(handle);
    int ret = AVERROR(EAGAIN);
    while (ret == AVERROR(EAGAIN))
    {
        int res = av_read_frame(self->ctx, self->pck);
        if (res < 0 && res != MY_EOF)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to read packet\n");
            return 0;
        }
        av_log(NULL, AV_LOG_INFO, "Packet pts: %ld\n", self->pck->pts);
        *stream_index = self->pck->stream_index;
        AVCodecContext *dec = self->streams[self->pck->stream_index].dec;
        AVFrame *frame = self->streams[self->pck->stream_index].cur;
        if (res != MY_EOF && avcodec_send_packet(dec, self->pck) < 0)
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to send packet\n");
            return 0;
        }
        ret = avcodec_receive_frame(dec, frame);
        if (ret == AVERROR(EAGAIN) && res == MY_EOF)
            return 0; //We've finished reading all frames from the file
        if (ret < 0 && ret != AVERROR(EAGAIN))
        {
            av_log(NULL, AV_LOG_ERROR, "Failed to decode frame: %d\n", ret);
            return 0;
        }
    }
    av_log(NULL, AV_LOG_INFO, "Frame pts: %ld\n", self->streams[self->pck->stream_index].cur->pts);
    return 1;
}

static int align_16(int in)
{
    while (in % 16 != 0)
        in += 1;
    return in;
}

static struct SwsContext *get_sws_context(decoder_t *dec, unsigned int stream_index)
{
    if (!dec->streams[stream_index].sws_ctx)
    {
        AVCodecContext *ctx = dec->streams[stream_index].dec;
        int stride = align_16(ctx->width * 3);
        uint8_t *buffer = av_malloc(stride * ctx->height);
        if (!buffer)
            return NULL;
        dec->streams[stream_index].rgb_buffer = buffer;
        dec->streams[stream_index].rgb_buffer_stride = stride;
        struct SwsContext *sws = sws_getContext(ctx->width, ctx->height, ctx->pix_fmt, ctx->width, ctx->height, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
        dec->streams[stream_index].sws_ctx = sws;
        return sws;
    }
    return dec->streams[stream_index].sws_ctx;
}

int ffmpeg_decode_frame_to_rgb(handle_t handle, unsigned int stream_index)
{
    DECODER(handle);
    AVCodecContext *ctx = self->streams[stream_index].dec;
    AVFrame *frame = self->streams[stream_index].cur;
    struct SwsContext *sws_ctx = get_sws_context(self, stream_index);
    if (!sws_ctx)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return 0;
    }
    uint8_t *dst_buffers[4] = {
            self->streams[stream_index].rgb_buffer,
            self->streams[stream_index].rgb_buffer,
            self->streams[stream_index].rgb_buffer,
            self->streams[stream_index].rgb_buffer
    };
    int dst_strides[4] = {
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride,
            self->streams[stream_index].rgb_buffer_stride
    };
    if (sws_scale(sws_ctx, (const uint8_t* const*)frame->data, frame->linesize, 0, ctx->height, dst_buffers, dst_strides) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Failed to convert frame to RGB\n");
        return 0;
    }
    return 1;
}

void ffmpeg_decode_get_rgb(handle_t handle, const rgb_pos_t *pos, rgb_t *out)
{
    DECODER(handle);
    uint8_t *buffer = self->streams[pos->stream_index].rgb_buffer;
    int stride = self->streams[pos->stream_index].rgb_buffer_stride;
    out->r = buffer[(pos->y * stride) + (pos->x * 3)];
    out->g = buffer[(pos->y * stride) + (pos->x * 3 + 1)];
    out->b = buffer[(pos->y * stride) + (pos->x * 3 + 2)];
}

handle_t ffmpeg_decode_new(const char *file)
{
    decoder_t *dec = alloc_decoder();
    if (dec == NULL)
        return NULL;
    if (avformat_open_input(&dec->ctx, file, NULL, NULL) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
        return NULL;
    }
    if (avformat_find_stream_info(dec->ctx, NULL) < 0)
    {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return NULL;
    }
    dec->pck = av_packet_alloc();
    if (!dec->pck)
    {
        av_log(NULL, AV_LOG_ERROR, "Memory allocation failure\n");
        return NULL;
    }
    return dec;
}

void ffmpeg_decode_delete(handle_t handle)
{
    DECODER(handle);
    if (!self)
        return;
    if (self->streams)
    {
        for (unsigned int i = 0; i < self->ctx->nb_streams; i++)
        {
            if (self->streams[i].sws_ctx)
            {
                sws_freeContext(self->streams[i].sws_ctx);
                av_free(self->streams[i].rgb_buffer);
            }
            av_frame_free(&self->streams[i].cur);
            avcodec_close(self->streams[i].dec);
        }
        av_free(self->streams);
    }
    av_packet_free(&self->pck);
    avformat_close_input(&self->ctx);
    av_free(self);
}

void ffmpeg_decode_get_frame_size(handle_t handle, unsigned int stream_index, unsigned int *w, unsigned int *h)
{
    DECODER(handle);
    *w = self->streams[stream_index].dec->width;
    *h = self->streams[stream_index].dec->height;
}

int64_t ffmpeg_decode_get_frame_timestamp(handle_t handle, unsigned int stream_index)
{
    DECODER(handle);
    return self->streams[stream_index].cur->pts;
}

void ffmpeg_decode_get_stream_props(handle_t handle, unsigned int stream_index, stream_properties_t *props)
{
    DECODER(handle);
    AVCodecContext *ctx = self->streams[stream_index].dec;
    props->time_base = self->ctx->streams[stream_index]->time_base;
    props->bit_rate = ctx->bit_rate;
    props->bit_rate_tolerance = ctx->bit_rate_tolerance;
    props->ticks_per_frame = ctx->ticks_per_frame;
    props->delay = ctx->delay;
    props->width = ctx->width;
    props->height = ctx->height;
    props->coded_width = ctx->coded_width;
    props->coded_height = ctx->coded_height;
    props->gop_size = ctx->gop_size;
    props->frame_rate = ctx->framerate;
    props->max_b_frames = ctx->max_b_frames;
    props->b_quant_factor = ctx->b_quant_factor;
    props->b_quant_offset = ctx->b_quant_offset;
    props->has_b_frames = ctx->has_b_frames;
    props->i_quant_factor = ctx->i_quant_factor;
    props->i_quant_offset = ctx->i_quant_offset;
}
