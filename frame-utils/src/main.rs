mod ffi;

use std::ffi::CString;
use std::fs::File;
use std::io::{Read, Write};
use std::os::raw::{c_int, c_uint};
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use std::process::exit;
use bpx::core::builder::{Checksum, CompressionMethod, MainHeaderBuilder, SectionHeaderBuilder};
use bpx::core::Container;
use bpx::sd::{Array, DebugSymbols, Object, Value};
use byteorder::{ByteOrder, LittleEndian};
use clap::{App, AppSettings, Arg};
use image::ImageBuffer;
use image::io::Reader;
use ffi::*;

const TIMESTAMPS_SECTION: u8 = b'T';
const METADATA_SECTION: u8 = b'M';

fn serialize_stream_props(props: &StreamProperties) -> Object
{
    let mut obj = Object::new();
    obj.set("time_base", vec![props.time_base.num, props.time_base.den].into());
    obj.set("bit_rate", props.bit_rate.into());
    obj.set("bit_rate_tolerance", props.bit_rate_tolerance.into());
    obj.set("ticks_per_frame", props.ticks_per_frame.into());
    obj.set("delay", props.delay.into());
    obj.set("width", props.width.into());
    obj.set("height", props.height.into());
    obj.set("coded_width", props.coded_width.into());
    obj.set("coded_height", props.coded_height.into());
    obj.set("gop_size", props.gop_size.into());
    obj.set("frame_rate", vec![props.frame_rate.num, props.frame_rate.den].into());
    obj.set("max_b_frames", props.max_b_frames.into());
    obj.set("b_quant_factor", props.b_quant_factor.into());
    obj.set("b_quant_offset", props.b_quant_offset.into());
    obj.set("has_b_frames", props.has_b_frames.into());
    obj.set("i_quant_factor", props.i_quant_factor.into());
    obj.set("i_quant_offset", props.i_quant_offset.into());
    let mut debug = DebugSymbols::new();
    debug.push("time_base");
    debug.push("bit_rate");
    debug.push("bit_rate_tolerance");
    debug.push("ticks_per_frame");
    debug.push("delay");
    debug.push("width");
    debug.push("height");
    debug.push("coded_width");
    debug.push("coded_height");
    debug.push("gop_size");
    debug.push("frame_rate");
    debug.push("max_b_frames");
    debug.push("b_quant_factor");
    debug.push("b_quant_offset");
    debug.push("has_b_frames");
    debug.push("i_quant_factor");
    debug.push("i_quant_offset");
    debug.write(&mut obj);
    obj
}

fn deserialize_rational(val: &Value) -> Rational
{
    let arr: &Array = val.try_into().unwrap();
    let useless = &arr[0];
    let useless2 = &arr[1];
    let num: c_int = useless.try_into().unwrap();
    let den: c_int = useless2.try_into().unwrap();
    Rational {
        num,
        den
    }
}

fn deserialize_props(obj: Object) -> StreamProperties
{
    let mut props = StreamProperties::default();
    props.time_base = deserialize_rational(obj.get("time_base").unwrap());
    props.bit_rate = obj.get("bit_rate").unwrap().try_into().unwrap();
    props.bit_rate_tolerance = obj.get("bit_rate_tolerance").unwrap().try_into().unwrap();
    props.ticks_per_frame = obj.get("ticks_per_frame").unwrap().try_into().unwrap();
    props.delay = obj.get("delay").unwrap().try_into().unwrap();
    props.width = obj.get("width").unwrap().try_into().unwrap();
    props.height = obj.get("height").unwrap().try_into().unwrap();
    props.coded_width = obj.get("coded_width").unwrap().try_into().unwrap();
    props.coded_height = obj.get("coded_height").unwrap().try_into().unwrap();
    props.gop_size = obj.get("gop_size").unwrap().try_into().unwrap();
    props.frame_rate = deserialize_rational(obj.get("frame_rate").unwrap());
    props.max_b_frames = obj.get("max_b_frames").unwrap().try_into().unwrap();
    props.b_quant_factor = obj.get("b_quant_factor").unwrap().try_into().unwrap();
    props.b_quant_offset = obj.get("b_quant_offset").unwrap().try_into().unwrap();
    props.has_b_frames = obj.get("has_b_frames").unwrap().try_into().unwrap();
    props.i_quant_factor = obj.get("i_quant_factor").unwrap().try_into().unwrap();
    props.i_quant_offset = obj.get("i_quant_offset").unwrap().try_into().unwrap();
    props
}

fn check_audio_video_stream(handle: Handle) -> (c_uint, c_uint)
{
    unsafe {
        let count = ffmpeg_decode_count_streams(handle);
        println!("Number of streams: {}", count);
        if count != 2 {
            eprintln!("Only videos with a single audio stream and a single video stream are supported!");
            ffmpeg_decode_delete(handle);
            exit(1);
        }
        let mut v = None;
        let mut v1 = None;
        for i in 0..count {
            if ffmpeg_decode_is_video_stream(handle, i) == 1 {
                v = Some(i);
            } else {
                v1 = Some(i);
            }
        }
        (v.expect("Couldn't find video stream index"), v1.expect("Couldn't find audio stream index"))
    }
}

fn decode_write_frames(src: &Path, outdir: &Path) -> (StreamProperties, Vec<i64>)
{
    let mut vec = Vec::new();
    let mut props = StreamProperties::default();
    unsafe {
        let s = CString::new(src.as_os_str().as_bytes()).unwrap();
        let handle = ffmpeg_decode_new(s.as_ptr());
        if handle == std::ptr::null() {
            exit(1);
        }
        if failed(ffmpeg_decode_init_streams(handle)) {
            exit(1);
        }
        let (idx_video_stream, _) = check_audio_video_stream(handle);
        ffmpeg_decode_get_stream_props(handle, idx_video_stream, &mut props as _);
        println!("Video stream time base is: {}/{}", props.time_base.num, props.time_base.den);
        let mut stream_index: c_uint = 0;
        let mut frame: usize = 0;
        while !failed(ffmpeg_decode_frame(handle, &mut stream_index)) {
            if stream_index != idx_video_stream {
                continue;
            }
            if failed(ffmpeg_decode_frame_to_rgb(handle, stream_index)) {
                exit(1);
            }
            let timestamp = ffmpeg_decode_get_frame_timestamp(handle, stream_index);
            vec.push(timestamp);
            let mut pos = RgbPos {
                stream_index,
                x: 0,
                y: 0
            };
            let mut rgb = Rgb::default();
            let mut img = ImageBuffer::new(props.width as _, props.height as _);
            for (x, y, pixel) in img.enumerate_pixels_mut() {
                pos.x = x as _;
                pos.y = y as _;
                ffmpeg_decode_get_rgb(handle, &pos, &mut rgb);
                *pixel = image::Rgb([rgb.r, rgb.g, rgb.b]);
            }
            println!("Saving frame #{} with timestamp {}", frame, timestamp);
            img.save(outdir.join(format!("frame{}.png", frame))).unwrap();
            frame += 1;
        }
        ffmpeg_decode_delete(handle);
    }
    (props, vec)
}

fn recompose_video(src: &Path, dir: &Path, out: &Path, props: &StreamProperties, timestamps: &Vec<i64>)
{
    unsafe {
        //Initialize the decoder to copy the audio stream
        let src_name = CString::new(src.as_os_str().as_bytes()).unwrap();
        let decoder = ffmpeg_decode_new(src_name.as_ptr());
        if decoder == std::ptr::null() {
            exit(1);
        }
        if failed(ffmpeg_decode_init_streams(decoder)) {
            exit(1);
        }
        let (_, idx_audio_stream) = check_audio_video_stream(decoder);

        let dst_name = CString::new(out.as_os_str().as_bytes()).unwrap();
        let handle = ffmpeg_encode_new(dst_name.as_ptr(), src_name.as_ptr());
        if handle == std::ptr::null() {
            exit(1);
        }
        let mut video_stream_index: c_uint = 0;
        let mut audio_stream_index: c_uint = 0;
        if failed(ffmpeg_encode_new_stream(handle, props as _, &mut video_stream_index as _)) {
            exit(1);
        }
        if failed(ffmpeg_encode_new_stream_from_existing(handle, decoder, idx_audio_stream, &mut audio_stream_index as _)) {
            exit(1);
        }
        if failed(ffmpeg_encode_write_header(handle)) {
            exit(1);
        }
        for (i, timestamp) in timestamps.iter().enumerate() {
            if failed(ffmpeg_encode_new_frame(handle, video_stream_index, *timestamp)) {
                exit(1);
            }
            let frame = Reader::open(dir.join(format!("frame{}.png", i))).expect("Failed to read frame").decode().expect("Failed to decode frame").into_rgb8();
            for (x, y, pix) in frame.enumerate_pixels() {
                let pos = RgbPos {x: x as _, y: y as _, stream_index: video_stream_index};
                let rgb = Rgb { r: pix[0], g: pix[1], b: pix[2] };
                ffmpeg_encode_set_rgb(handle, &pos as _, &rgb as _);
            }
            println!("Writing frame #{} with timestamp {}", i, *timestamp);
            if failed(ffmpeg_encode_write_frame(handle, video_stream_index)) {
                exit(1);
            }
        }
        if failed(ffmpeg_encode_flush(handle, video_stream_index)) {
            exit(1);
        }
        if failed(ffmpeg_encode_copy_streams(handle, decoder, idx_audio_stream, audio_stream_index)) {
            exit(1);
        }
        if failed(ffmpeg_encode_write_trailer(handle)) {
            exit(1);
        }
        ffmpeg_encode_delete(handle);
    }
}

fn main()
{
    let matches = App::new("frame-utils")
        .author("Yuri Edward")
        .about("FrameUtils")
        .version("1.0.0")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(App::new("decompose")
            .args([
                Arg::new("output").short('o').long("output").takes_value(true).required(true)
                    .help("Output directory"),
                Arg::new("input").short('i').long("input").takes_value(true).required(true)
                    .help("Input video file")
            ])
        )
        .subcommand(App::new("recompose")
            .args([
                Arg::new("output").short('o').long("output").takes_value(true).required(true)
                    .help("Output video file"),
                Arg::new("input").short('i').long("input").takes_value(true).required(true)
                    .help("Input directory"),
                Arg::new("source").short('s').long("src").takes_value(true).required(true)
                    .help("Source video to copy audio track from"),
                Arg::new("scale").long("scale").takes_value(true)
                    .help("New video scale (default = 1)"),
            ])
        )
        .get_matches();
    if let Some(val) = matches.subcommand_matches("recompose") {
        let scale = val.value_of_t("scale").unwrap_or(1);
        let output = Path::new(val.value_of_os("output").unwrap());
        let input = Path::new(val.value_of_os("input").unwrap());
        let src = Path::new(val.value_of_os("source").unwrap());
        if output.is_dir() {
            eprintln!("Output is a directory!");
            exit(1);
        }
        let mut timestamps = Vec::new();
        let mut props = StreamProperties::default();
        let f = File::open(input.join("database.bpx")).expect("Couldn't open database BPX");
        let mut container = Container::open(f).expect("Couldn't load database BPX");
        for mut section in container.iter_mut()
        {
            match section.btype {
                TIMESTAMPS_SECTION => {
                    let data = section.load().expect("Failed to load timestamps section");
                    let mut buf: [u8; 8] = [0; 8];
                    while data.read(&mut buf).expect("Error reading timestamps") > 0 {
                        timestamps.push(LittleEndian::read_i64(&buf));
                    }
                },
                METADATA_SECTION => {
                    let data = section.load().expect("Failed to load metadata section");
                    let obj = Object::read(data).expect("Failed to decode metadata BPXSD Object");
                    props = deserialize_props(obj);
                },
                _ => ()
            }
        }
        props.width *= scale;
        props.height *= scale;
        props.coded_height *= scale;
        props.coded_width *= scale;
        recompose_video(src, input, output, &props, &timestamps);
    }
    if let Some(val) = matches.subcommand_matches("decompose") {
        let output = Path::new(val.value_of_os("output").unwrap());
        let input = Path::new(val.value_of_os("input").unwrap());
        if output.is_file() {
            eprintln!("Output is a file!");
            exit(1);
        }
        std::fs::create_dir(output).expect("Failed to create directory");
        let (props, timestamps) = decode_write_frames(input, output);
        let f = File::create(output.join("database.bpx")).expect("Failed to create database file");
        let mut container = Container::create(f, MainHeaderBuilder::new()
            .with_type(b'U') //use 'U' for Undefined
            .with_version(2)
        );
        let htimestamps = container.create_section(SectionHeaderBuilder::new()
            .with_type(TIMESTAMPS_SECTION)
            .with_compression(CompressionMethod::Zlib)
            .with_checksum(Checksum::Weak));
        let hmetadata = container.create_section(SectionHeaderBuilder::new()
            .with_type(METADATA_SECTION)
            .with_compression(CompressionMethod::Zlib)
            .with_checksum(Checksum::Weak));
        {
            let mut section = container.get_mut(htimestamps);
            let data = section.open().unwrap();
            for v in timestamps {
                let mut buf: [u8; 8] = [0; 8];
                LittleEndian::write_i64(&mut buf, v);
                data.write(&buf).expect("Failed to write timestamp");
            }
        }
        {
            let mut section = container.get_mut(hmetadata);
            let obj = serialize_stream_props(&props);
            obj.write(section.open().unwrap()).expect("Failed to encode metadata BPXSD Object");
        }
        container.save().expect("Couldn't save BPX container");
    }
}
