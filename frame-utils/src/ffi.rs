use std::ffi::c_void;
use std::os::raw::{c_char, c_float, c_int, c_uint};

#[repr(C)]
pub struct RgbPos
{
    pub stream_index: c_uint,
    pub x: c_int,
    pub y: c_int
}

#[derive(Default)]
#[repr(C)]
pub struct Rgb
{
    pub r: u8,
    pub g: u8,
    pub b: u8
}

#[derive(Default)]
#[repr(C)]
pub struct Rational
{
    pub num: c_int,
    pub den: c_int
}

#[derive(Default)]
#[repr(C)]
pub struct StreamProperties
{
    pub time_base: Rational,
    pub bit_rate: i64,
    pub bit_rate_tolerance: c_int,
    pub ticks_per_frame: c_int,
    pub delay: c_int,
    pub width: c_int,
    pub height: c_int,
    pub coded_width: c_int,
    pub coded_height: c_int,
    pub gop_size: c_int,
    pub frame_rate: Rational,
    pub max_b_frames: c_int,
    pub b_quant_factor: c_float,
    pub b_quant_offset: c_float,
    pub has_b_frames: c_int,
    pub i_quant_factor: c_float,
    pub i_quant_offset: c_float
}

pub type Handle = *const c_void;

pub fn failed(res: c_int) -> bool
{
    res == 0
}

extern "C"
{
    // Decoder API
    pub fn ffmpeg_decode_new(file: *const c_char) -> Handle;
    pub fn ffmpeg_decode_init_streams(handle: Handle) -> c_int;
    pub fn ffmpeg_decode_is_video_stream(handle: Handle, index: c_uint) -> c_int;
    pub fn ffmpeg_decode_frame(handle: Handle, stream_index: *mut c_uint) -> c_int;
    pub fn ffmpeg_decode_frame_to_rgb(handle: Handle, stream_index: c_uint) -> c_int;
    pub fn ffmpeg_decode_get_rgb(handle: Handle, pos: *const RgbPos, out: *mut Rgb);
    pub fn ffmpeg_decode_get_frame_size(handle: Handle, stream_index: c_uint, w: *mut c_uint, h: *mut c_uint);
    pub fn ffmpeg_decode_delete(handle: Handle);
    pub fn ffmpeg_decode_count_streams(handle: Handle) -> c_uint;
    pub fn ffmpeg_decode_get_frame_timestamp(handle: Handle, stream_index: c_uint) -> i64;
    pub fn ffmpeg_decode_get_stream_props(handle: Handle, stream_index: c_uint, props: *mut StreamProperties);

    // Encoder API
    pub fn ffmpeg_encode_new(file: *const c_char, format: *const c_char) -> Handle;
    pub fn ffmpeg_encode_new_stream(handle: Handle, props: *const StreamProperties, stream_index: *mut c_uint) -> c_int;
    pub fn ffmpeg_encode_write_header(handle: Handle) -> c_int;
    pub fn ffmpeg_encode_write_trailer(handle: Handle) -> c_int;
    pub fn ffmpeg_encode_new_frame(handle: Handle, stream_index: c_uint, timestamp: i64) -> c_int;
    pub fn ffmpeg_encode_write_frame(handle: Handle, stream_index: c_uint) -> c_int;
    pub fn ffmpeg_encode_flush(handle: Handle, stream_index: c_uint) -> c_int;
    pub fn ffmpeg_encode_set_rgb(handle: Handle, pos: *const RgbPos, rgb: *const Rgb);
    pub fn ffmpeg_encode_delete(handle: Handle);
    pub fn ffmpeg_encode_new_stream_from_existing(handle: Handle, decoder: Handle, in_stream_index: c_uint, out_stream_index: *mut c_uint) -> c_int;
    pub fn ffmpeg_encode_copy_streams(handle: Handle, decoder: Handle, in_stream_index: c_uint, out_stream_index: c_uint) -> c_int;
}
