fn main()
{
    println!("cargo:rustc-link-lib=dylib=avutil");
    println!("cargo:rustc-link-lib=dylib=swresample");
    println!("cargo:rustc-link-lib=dylib=swscale");
    println!("cargo:rustc-link-lib=dylib=avcodec");
    println!("cargo:rustc-link-lib=dylib=avformat");
    println!("cargo:rustc-link-lib=dylib=CInterface");
    println!("cargo:rustc-link-search=native=../CInterface");
    println!("cargo:rustc-link-search=native=../CInterface/FFMPEG/lib");
    println!("cargo:rerun-if-changed=../CInterface/libCInterface.so");
    #[cfg(debug_assertions)]
    println!("cargo:rustc-link-arg=-Wl,-rpath=../CInterface/");
    #[cfg(not(debug_assertions))]
    println!("cargo:rustc-link-arg=-Wl,-rpath=$ORIGIN");
}